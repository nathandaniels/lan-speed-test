﻿using System;

namespace LanSpeedTest
{
    public class Progress
    {
        public Progress(long bytesTransmitted, long totalBytes, Speed speedOverLastSecond, Speed speedOverLastMinute, Speed speedOverall)
        {
            BytesTransmitted = bytesTransmitted;
            TotalBytes = totalBytes;
            SpeedOverLastSecond = speedOverLastSecond;
            SpeedOverLastMinute = speedOverLastMinute;
            SpeedOverall = speedOverall;
        }

        public Int64 BytesTransmitted { get; }

        public Int64 TotalBytes { get; }

        public Single Percent => this.BytesTransmitted / (Single)this.TotalBytes;

        public Speed SpeedOverLastSecond { get; }
        public Speed SpeedOverLastMinute { get; }
        public Speed SpeedOverall { get; }
    }
}
