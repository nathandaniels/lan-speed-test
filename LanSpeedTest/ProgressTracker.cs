﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace LanSpeedTest
{
    public class ProgressTracker: IDisposable
    {
        private LinkedList<Checkpoint> Checkpoints { get; } = new LinkedList<Checkpoint>();

        private DateTime LastInterval { get; set; }

        private DateTime StartTime { get; set; }

        private ReaderWriterLockSlim Lock { get; } = new ReaderWriterLockSlim();

        public Progress GetProgress()
        {
            var now = DateTime.Now;
            var secondAgo = now - TimeSpan.FromSeconds(1);
            var minuteAgo = now - TimeSpan.FromSeconds(60);
            Checkpoint 

            this.Lock.EnterReadLock();

            foreach (var cp in this.Checkpoints)
            {

            }

            this.Lock.ExitReadLock();
        }

        public void Dispose()
        {
            this.Lock?.Dispose();
        }

        public void BytesRead(UInt64 bytes)
        {
            var last = new Checkpoint(bytes, DateTime.Now);
            this.Lock.EnterWriteLock();
            this.Checkpoints.AddLast(last);
            this.Lock.ExitWriteLock();
        }

        private class Checkpoint
        {
            public Checkpoint(ulong bytes, DateTime time)
            {
                Bytes = bytes;
                Time = time;
            }

            public UInt64 Bytes { get; }
            public DateTime Time { get; }
        }
    }
}
