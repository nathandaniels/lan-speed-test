﻿using System;

namespace LanSpeedTest
{
    /// <summary>
    /// Represents network speed and all of the many ways to represent it
    /// </summary>
    public struct Speed
    {
        public Speed(long bytes, TimeSpan timeFrame, bool useBits, bool useBinaryPrefix) : this()
        {
            Bytes = bytes;
            TimeFrame = timeFrame;
            UseBits = useBits;
            UseBinaryPrefix = useBinaryPrefix;
        }

        public Speed(long bytes, TimeSpan timeFrame) : this()
        {
            Bytes = bytes;
            TimeFrame = timeFrame;
        }

        public Int64 Bytes { get; set; }

        public TimeSpan TimeFrame { get; set; }

        public Boolean UseBits { get; set; }

        public Boolean UseBinaryPrefix { get; set; }

        public Int64 Units => (this.UseBits ? 8 : 1) * this.Bytes;

        public Double KiloUnits => this.Units / this.Multiple;

        public Double MegaUnits => this.KiloUnits / this.Multiple;

        public Double GigaUnits => this.MegaUnits / this.Multiple;

        public Double UnitsPerSecond => this.PerSecond(this.Units);

        public Double KiloUnitsPerSecond => this.PerSecond(this.KiloUnits);

        public Double MegaUnitsPerSecond => this.PerSecond(this.MegaUnits);

        public Double GigaUnitsPerSecond => this.PerSecond(this.GigaUnits);

        public String UnitsPerSecondString => $"{this.UnitsPerSecond} {this.UnitLabel}";

        public String KiloUnitsPerSecondString => $"{this.KiloUnitsPerSecond} k{this.UnitLabel}";

        public String MegaUnitsPerSecondString => $"{this.MegaUnitsPerSecond} M{this.UnitLabel}";

        public String GigaUnitsPerSecondString => $"{this.GigaUnitsPerSecond} G{this.UnitLabel}";

        public String MostAppropriatePerSecondString
        {
            get
            {
                String GetIfGood(Double? value, Double multiple, String result) => value > multiple ? result : null;

                return GetIfGood(this.Units, this.Multiple, this.UnitsPerSecondString)
                    ?? GetIfGood(this.Units, this.Multiple, this.KiloUnitsPerSecondString)
                    ?? GetIfGood(this.Units, this.Multiple, this.MegaUnitsPerSecondString)
                    ?? this.GigaUnitsPerSecondString;
            }
        }

        private Double Multiple => this.UseBinaryPrefix ? 1024 : 1000;

        private String UnitLabel => $"{(this.UseBinaryPrefix ? "i" : String.Empty)}{(this.UseBits ? 'b' : 'B')}";

        private Double PerSecond(Double units) => units / this.TimeFrame.TotalSeconds;

        public override String ToString() => this.MostAppropriatePerSecondString;
    }
}
